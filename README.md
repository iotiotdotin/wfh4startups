# Wfh4startups

The motivation for the Work From Home for Startups project is to provide a 
one stop solution to needs of the Startup that are Working from Home in
the COVID-19 pandemic.

## What would an Ideal solution provide ? 

1.	**Manage Teams using Agile**		
	- Manage S/W Development Teams, each team sees projects that they are working on	
	- Manage S/W Projects, code and its dependencies	
	- Visualize & Group, Tasks	
	- Keep track of Features and Bugs in the code 	
	- Discuss Features/Bugs in detail 	
	- Track time taken for each Task	
	- Manage Agile Sprints and keep a track of the software development	
2.	**Manage code**		
	- Version control Code using Git	
	- Easy Code Review and Suggest improvements to the code on same page using Merge Requests	
	- Automate S/W Build, Test and Release using CI/CD	
	- Document projects using built-in Wiki	
	- Built-in Docker container registry for S/W deployment and testing on various servers	
3.	**Communicate**		
	- Internal org video calling for Daily scrum meet using BBB	
4.	**Administration**		
	- Manage Employee permissions to Projects	
	- Automated Backup's and Easy Restores so you do not loose data 	
5.	**Easily Migrate from Github, Bitbucket private repo's**		
6.	**Integrations**		
	- Slack Notifications and Slash commands for Projects	
	- Commits to Asana Task comments for Projects	
	- Email commit diffs on push for each Projects	

## Feel that some features are missing, Lets Discuss your thoughts [here](https://gitlab.com/iotiotdotin/wfh4startups/-/issues/1)
